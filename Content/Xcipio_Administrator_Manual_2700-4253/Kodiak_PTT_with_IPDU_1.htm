﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/Important_2.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../Resources/Stylesheets/Stylesheet_SS8.css" rel="stylesheet" />
    </head>
    <body>
        <h4 class="Head_4"><a name="network_configuration_examples_608548803_2763027"></a>Kodiak PTT with IPDU</h4>
        <p class="Figure">Kodiak PTT with IPDU Flowchart</p>
        <p>
            <img src="KODIAK_PTT flowchart with IPDU only1.jpg" style="width: 99.19967px;height: 508.7991px;margin-left: 161.4002px;margin-top: 4.19832px;margin-right: 160.2px;margin-bottom: 0.09934855px;" />
        </p>
        <p class="Body_Text_1">To configure AF and CF interfaces for the Kodiak PTT, proceed as follows. </p>
        <table style="width: 624px;caption-side: top;mc-table-style: url('../Resources/TableStyles/Important_2.css');" class="TableStyle-Important" cellspacing="0">
            <col width="96px" class="TableStyle-Important-Column-Column1" />
            <col width="528px" class="TableStyle-Important-Column-Column1" />
            <tbody>
                <tr class="TableStyle-Important-Body-Body1">
                    <td class="TableStyle-Important-BodyB-Column1-Body1">
                        <p class="Table_Head_Left_Small">Important!</p>
                    </td>
                    <td class="TableStyle-Important-BodyA-Column1-Body1">
                        <p class="Table_Body_Left_1">Make certain all Disaster Recovery Day One provisioning is completed prior to provisioning this.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="Body_Text_1">Make certain the DFNI has already been configured.</p>
        <p class="Body_Text_1">Add contacts, if necessary:</p>
        <p class="Screen_Text_2">add-contact:nickname=holmes,org=interpol,phone=8005551234;</p>
        <p class="Body_Text_1">Configure the Network:</p>
        <p class="Body_Text_2">Add a network that describes each component in the network topology. Ideally, all Xcipio components’ addresses and ports should be represented to allow the administrator to see all ports and addresses at a glance.</p>
        <p class="Screen_Text_2">add-network:netid=xcpnet,ownaddr=2001::200:f8ff:fe21:60ef;</p>
        <p class="Body_Text_1">Add an IPDU:</p>
        <p class="Screen_Text_2">add-ipdu:ipduid=1,ipaddr=2001::200:f8ff:fe21:60ef,switchtech=F;</p>
        <p class="Body_Text_1">Add Security Information (optional):</p>
        <p class="Body_Text_2">Only one SECINFO object can exist per system. </p>
        <p class="Body_Text_2">Security is required for KODIAK_PTT.</p>
        <p class="Screen_Text_2">add-secinfo:id=abc123,inttype=TLS,tlstype=SERVER,authtype=SIMPLE,<br />passphrase=admin123,keyfile=privatecert,owncertfile=owncert,<br />certdir=/SS8/$PRODUCT/$VERSION/RUN/config/CERT;</p>
        <p class="Body_Text_1">Add an access function:</p>
        <p class="Screen_Text_2">add-af:afid=kchat1,contact=HOLMES,name=KChat,type=KODIAK_PTT,<br />tz=US/Eastern,version=2;</p>
        <p class="Screen_Text_2">add-af:afid=kchat2,contact=HOLMES,name=KChat,type=KODIAK_PTT,<br />tz=US/Eastern,version=2;</p>
        <p class="Body_Text_1">Add a JAREA if desired:</p>
        <p class="Screen_Text_2">add-jarea:id=CA6,afid=kchat1;</p>
        <p class="Screen_Text_2">add-jarea:id=CA6,afid=kchat2;</p>
        <p class="Body_Text_1">Add an access function provisioning interface:</p>
        <p class="Body_Text_2">Configure two, one for each location.</p>
        <p class="Screen_Text_2">add-rtxafpi:afid=kchat1,ifid=1,reqstate=ACTIVE,hostname=Kodiak9,<br />username=NYC16,password=kchat123,rmtusername=SanJose2,<br />rmtpassword=kchat567,dscp=32;</p>
        <p class="Screen_Text_2">add-rtxafpi:afid=kchat2,ifid=1,reqstate=ACTIVE,hostname=Kodiak9,<br />username=NYC16,password=kchat123,rmtusername=SanJose2,<br />rmtpassword=kchat567,dscp=32;</p>
        <p class="Screen_Text_2">add-rtxafpi:afid=kchat1,ifid=2,reqstate=ACTIVE,hostname=Kodiak16,<br />username=SanJose2,password=kchat567,rmtusername=NYC16,<br />rmtpassword=kchat123,dscp=32;</p>
        <p class="Screen_Text_2">add-rtxafpi:afid=kchat2,ifid=2,reqstate=ACTIVE,hostname=Kodiak16,<br />username=SanJose2,password=kchat567,rmtusername=NYC16,<br />rmtpassword=kchat123,dscp=32;</p>
        <p class="Body_Text_1">Add an access function call data interface:</p>
        <p class="Body_Text_2">Only add SECINFOID if security is required.</p>
        <p class="Body_Text_2">Configure two, one for each location.</p>
        <p class="Screen_Text_2">add-t1678afdi:afid=kchat1,ifid=1,ipaddr=2001::200:f8ff:fe19:68ef,<br />netid=xcpnet,port=10010,reqstate=ACTIVE,persistent=Y,secinfoid=abc123,<br />dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afdi:afid=kchat2,ifid=1,ipaddr=2001::200:f8ff:fe19:31ae,<br />netid=xcpnet,port=10010,reqstate=ACTIVE,persistent=Y,secinfoid=abc123,<br />dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afdi:afid=kchat1,ifid=2,ipaddr=2001::200:f6aa:fe21:67ef,<br />netid=xcpnet,port=10010,reqstate=ACTIVE,persistent=Y,secinfoid=abc123,<br />dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afdi:afid=kchat2,ifid=2,ipaddr=2001::200:f6aa:fe21:22bc,<br />netid=xcpnet,port=10010,reqstate=ACTIVE,persistent=Y,secinfoid=abc123,<br />dscp=16;</p>
        <p class="Body_Text_1">Add an access function call content interface:</p>
        <p class="Body_Text_2">Only add SECINFOID if security is required.</p>
        <p class="Body_Text_2">Configure two, one for each location.</p>
        <p class="Screen_Text_2">add-t1678afci:afid=kchat1,ifid=1,ipaddr=2001::200:f8ff:fe19:65ef,<br />netid=xcpnet,port=0,secinfoid=abc123,dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afci:afid=kchat2,ifid=1,ipaddr=2001::200:f8ff:fe19:66ef,<br />netid=xcpnet,port=0,secinfoid=abc123,dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afci:afid=kchat1,ifid=2,ipaddr=2001::200:f6aa:fe21:71ab,<br />netid=xcpnet,port=0,secinfoid=abc123,dscp=16;</p>
        <p class="Screen_Text_2">add-t1678afci:afid=kchat2,ifid=2,ipaddr=2001::200:f6aa:fe21:72ab,<br />netid=xcpnet,port=0,secinfoid=abc123,dscp=16;</p>
        <p class="Body_Text_1">Add a collection function:</p>
        <p class="Screen_Text_2">add-cf:cfid=CFID1,name=CF1,type=TIA1072,insidenat=N,tz=US/EASTERN, celltowerlocation=Y;</p>
        <p class="Body_Text_1">Add a collection function delivery interface:</p>
        <p class="Screen_Text_2">add-t1678cfdi:cfid=CFID1,ifid=1,destip=2001::200:f8ff:fe21:15aa,<br />destport=14000,dscp=16,version=TIA1072,secinfoid=abc123;</p>
        <p class="Body_Text_1">For Surveillance interception, see the <span class="Document">Xcipio Operator Manual</span>.</p>
    </body>
</html>